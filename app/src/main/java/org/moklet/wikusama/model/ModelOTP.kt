package org.moklet.wikusama.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

object ModelOTP {

    @Parcelize
    data class RequestSendOTP(
            @SerializedName("phoneNumber") val msisdn: String?) : Parcelable

    @Parcelize
    data class ResponseSendOTP(
            @SerializedName("data") val data: Data?,
            @SerializedName("message") val message: String?,
            @SerializedName("status") val status: String?) : Parcelable

    @Parcelize
    data class RequestVerifyOTP(
            @SerializedName("sessionID") val msisdn: String?,
            @SerializedName("otp") val otp: String?) : Parcelable

    @Parcelize
    data class Data(
            @SerializedName("phoneNumber") val phoneNumber: String?,
            @SerializedName("sessionID") val sessionID: String?) : Parcelable

    @Parcelize
    data class ResponseVerifyOTP(
            @SerializedName("data") val data: String?,
            @SerializedName("message") val message: String?,
            @SerializedName("status") val status: String?) : Parcelable


}
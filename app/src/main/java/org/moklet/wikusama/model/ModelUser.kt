package org.moklet.wikusama.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

object ModelUser {

    @Parcelize
    data class Profile(
            @SerializedName("name")                 var name: String?,
            @SerializedName("gender")               var gender: String?,
            @SerializedName("bod")                  var bod: String?,
            @SerializedName("email")                var email: String?,
            @SerializedName("pathImage")            var pathImage: String?,
            @SerializedName("bio")                  var biography: String?) : Parcelable


}
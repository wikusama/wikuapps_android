package org.moklet.wikusama.custom.widget

import android.content.Context
import android.os.Bundle
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.moklet.wikusama.R
import org.moklet.wikusama.utilities.android.UtilConstant
import kotlinx.android.synthetic.main.dialog_bottom_loading.*

class WidgetBottomProgress(context: Context) : BottomSheetDialog(context) {

    private var indicatorAnimation: AnimatedVectorDrawableCompat?   = null
    private var progressLoadingMessage: String? = UtilConstant.WIKU_INITIAL_EMPTY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(false)
        setCanceledOnTouchOutside(true)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        showProgressLoading()
        showProgressMessage(progressLoadingMessage)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        dismissProgressLoading()
    }

    override fun setTitle(title: CharSequence?) {
        super.setTitle(title)
        progressLoadingMessage = title as String?
    }

    private fun showProgressMessage(message : String?) {
        views_bottom_loading_status_textview?.text = message
    }

    private fun dismissProgressLoading() {
        indicatorAnimation?.stop()
        indicatorAnimation = null
    }

    private fun showProgressLoading() {
        context?.let {
            indicatorAnimation = AnimatedVectorDrawableCompat.create(it, R.drawable.al_loading_content_black_24dp)
            view_bottom_loading_progress?.setImageDrawable(indicatorAnimation)
            indicatorAnimation?.start()
        }
    }

}
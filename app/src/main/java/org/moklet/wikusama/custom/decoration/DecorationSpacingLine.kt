package org.moklet.wikusama.custom.decoration

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat

class DecorationSpacingLine(context: Context?, @DrawableRes dividerResource : Int) : androidx.recyclerview.widget.RecyclerView.ItemDecoration() {

    private val divider: Drawable? = context?.let { ContextCompat.getDrawable(it, dividerResource) }

    override fun onDrawOver(c: Canvas, parent: androidx.recyclerview.widget.RecyclerView, state: androidx.recyclerview.widget.RecyclerView.State) {
        val left        = parent.paddingLeft
        val right       = parent.width - parent.paddingRight
        val childCount  = parent.childCount

        for (i in 0 until childCount - 1) {
            val child   = parent.getChildAt(i)
            val params  = child.layoutParams as androidx.recyclerview.widget.RecyclerView.LayoutParams
            val top     = child.bottom + params.bottomMargin
            val bottom  = top + (divider?.intrinsicHeight ?: 0)
            divider?.setBounds(left, top, right, bottom)
            divider?.draw(c)
        }
    }
}
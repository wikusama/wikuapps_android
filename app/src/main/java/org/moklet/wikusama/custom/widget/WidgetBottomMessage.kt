package org.moklet.wikusama.custom.widget

import android.content.Context
import android.os.Bundle
import org.moklet.wikusama.utilities.android.UtilConstant
import kotlinx.android.synthetic.main.dialog_bottom_message.*

class WidgetBottomMessage(context: Context) : com.google.android.material.bottomsheet.BottomSheetDialog(context) {

    private var progressLoadingMessage: String? = UtilConstant.WIKU_INITIAL_EMPTY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(false)
        setCanceledOnTouchOutside(true)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        showMessageError(progressLoadingMessage)
    }

    override fun setTitle(title: CharSequence?) {
        super.setTitle(title)
        progressLoadingMessage = title as String?
    }

    override fun onContentChanged() {
        super.onContentChanged()
        showMessageError(progressLoadingMessage)
    }

    private fun showMessageError(message : String?) {
        view_bottom_error_status_textview?.text = message
    }

}
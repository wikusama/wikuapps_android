package org.moklet.wikusama.feature.core

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

abstract class CorePresenter<T : CoreView> {

    val job                                 = Job()
    val uiDispatcher: CoroutineDispatcher   = Dispatchers.Main
    var viewLayer : T?                      = null

    fun detachView() { job.cancel(); viewLayer = null }
    fun attachView(view : T) { viewLayer = view }

}
package org.moklet.wikusama.feature.presenter

import org.moklet.wikusama.feature.contract.ContractRegister
import org.moklet.wikusama.feature.core.CorePresenter
import org.moklet.wikusama.repository.RepositorySession
import org.moklet.wikusama.utilities.android.UtilFormat

class PresenterRegister(private var repositorySession: RepositorySession)
    : CorePresenter<ContractRegister.View>(), ContractRegister.Action {

    override fun requestOneTimePassVerification(phoneNumber: String?) {
        if (UtilFormat.isValidEmail(phoneNumber!!)) {
            repositorySession.userEmail = phoneNumber
            viewLayer?.openOneTimePassVerification()
            return
        }
        viewLayer?.displayEmailNotValid()
    }

}
package org.moklet.wikusama.feature.core

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import butterknife.ButterKnife
import org.moklet.wikusama.R
import org.moklet.wikusama.custom.widget.WidgetBottomMessage
import org.moklet.wikusama.custom.widget.WidgetBottomProgress
import org.moklet.wikusama.utilities.android.UtilConstant

@SuppressLint("Registered")
abstract class CoreActivity : AppCompatActivity() {

    companion object {
        const val CLASS_TAG = "Core Activity"
    }

    private var widgetBottomProgress: WidgetBottomProgress? = null
    private val notificationAction = IntentFilter(UtilConstant.WIKU_ACTION_NOTIFICATION)
    private var widgetBottomMessage: WidgetBottomMessage? = null

    private val notificationService = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            generateNotification(intent)
        }
    }

    @LayoutRes
    protected abstract fun getLayoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        ButterKnife.bind(this)
    }


    fun generateNotification(intent: Intent) {
        when (intent.getStringExtra(UtilConstant.WIKU_ARGUMENT_TYPE)) {

        }
    }

    fun showBottomProgress(message: String? = UtilConstant.WIKU_INITIAL_EMPTY) {
        widgetBottomProgress = WidgetBottomProgress(this)
        widgetBottomProgress?.setTitle(getString(R.string.WIKU_TITLE_LOADING))
        widgetBottomProgress?.setContentView(R.layout.dialog_bottom_loading)
        widgetBottomProgress?.findViewById<AppCompatTextView>(R.id.views_bottom_loading_status_textview)?.text = message
        widgetBottomMessage?.dismiss()
        widgetBottomProgress?.show()
    }

    fun showErrorMessage(message: String?) {
        checkNotNull(this) { return }
        val messageInfo: String = if (message.isNullOrBlank()) {
            this.getString(R.string.WIKU_TITLE_ERROR_PARSING) ?: UtilConstant.WIKU_INITIAL_EMPTY
        } else {
            message
        }
        widgetBottomMessage = WidgetBottomMessage(this)
        widgetBottomMessage?.setTitle(messageInfo)
        widgetBottomMessage?.setContentView(R.layout.dialog_bottom_message)
        widgetBottomMessage?.findViewById<AppCompatTextView>(R.id.view_bottom_error_status_textview)?.text = messageInfo
        widgetBottomProgress?.dismiss()
        widgetBottomMessage?.show()
    }

    fun dismissBottomProgress() {
        widgetBottomProgress?.dismiss()
        widgetBottomProgress = null
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(baseContext).registerReceiver(notificationService, notificationAction)
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(baseContext).unregisterReceiver(notificationService)
        super.onPause()
    }

    override fun onDestroy() {
        dismissBottomProgress()
        super.onDestroy()
    }

}
package org.moklet.wikusama.feature.presenter

import android.location.Location
import org.moklet.wikusama.controller.ControllerEndpoint
import org.moklet.wikusama.feature.contract.ContractDashboard
import org.moklet.wikusama.feature.core.CorePresenter
import org.moklet.wikusama.repository.RepositorySession

class PresenterDashboard(private var repositorySession: RepositorySession,
                         private var controllerEndpoint: ControllerEndpoint) : CorePresenter<ContractDashboard.View>(), ContractDashboard.Action {

    override var currentLocation: Location? = null


    override fun requestInformationCollection() {
//        viewLayer?.displayCollectionInformation()
    }


}
package org.moklet.wikusama.feature.screen.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import butterknife.OnClick
import butterknife.OnTextChanged
import org.moklet.wikusama.R
import org.moklet.wikusama.feature.contract.ContractOTP
import org.moklet.wikusama.feature.core.CoreActivity
import org.moklet.wikusama.feature.presenter.PresenterOTP
import org.moklet.wikusama.feature.screen.dialog.DialogResendOTP
import org.moklet.wikusama.utilities.android.UtilConstant
import kotlinx.android.synthetic.main.activity_one_time_pass.*
import org.koin.android.ext.android.inject

open class ActivityOTP : CoreActivity(), ContractOTP.View, DialogResendOTP.DialogResendOTPCallback {

    companion object {
        const val LAYOUT_RESOURCE = R.layout.activity_one_time_pass
        const val CLASS_TAG = "Activity One Time Pass"

        fun startOneTimePass(context: Context) {
            val intentOneTimePass = Intent(context, ActivityOTP::class.java)
            intentOneTimePass.action = UtilConstant.WIKU_ACTION_ONE_TIME_PASS
            context.startActivity(intentOneTimePass)
        }
    }

    override fun getLayoutId(): Int = LAYOUT_RESOURCE
    private val presenterOTP: PresenterOTP by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenterOTP.attachView(this)
        initializeContent()
    }

    private fun initializeContent() {
        presenterOTP.loadDestinationPhoneNumber()
    }

    @OnTextChanged(R.id.view_support_otp_code_edittext, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected fun onOneTimePassFieldChanged(editable: Editable) {
        if (editable.length == 6) presenterOTP.requestVerifyOneTimePass(editable.toString())
    }

    @OnClick(R.id.view_register_action_resend_otp_textview)
    protected fun onOnTimePassResendSelected() {
        presenterOTP.openResendOneTimePass()
    }

    @SuppressLint("SetTextI18n")
    override fun displayPhoneNumber(phoneNumber: String?) {
        view_register_desc_text_view?.text = "Sent to $phoneNumber"
    }

    override fun displayResendOneTimePass(phoneNumber: String?) {
        DialogResendOTP.newInstance(null, CLASS_TAG, UtilConstant.WIKU_ACTION_RESEND_OTP, phoneNumber).show(supportFragmentManager, DialogResendOTP.CLASS_TAG)
    }

    override fun displayOneTimePass(oneTimePass: String?) {
        view_support_otp_code_edittext?.setText(oneTimePass)
    }

    override fun displayContentLoadingProgress() {
        showBottomProgress()
    }

    override fun dismissContentLoadingProgress() {
        dismissBottomProgress()
    }

    override fun displayOneTimePassNotVerify() {
        view_support_otp_code_edittext?.text?.clear()
    }

    override fun onResendByPhoneSelected() {
        presenterOTP.requestSendOneTimePass()
    }

    override fun openDashboard() {
        ActivityDashboard.startDashboard(this)
        finish()
    }


    override fun onDestroy() {
        presenterOTP.detachView()
        super.onDestroy()
    }


}
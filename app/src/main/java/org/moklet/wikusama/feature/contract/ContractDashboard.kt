package org.moklet.wikusama.feature.contract

import android.location.Location
import org.moklet.wikusama.feature.core.CoreView
import org.moklet.wikusama.feature.core.CoreAction
import org.moklet.wikusama.model.ModelUser

object ContractDashboard {

    interface View : CoreView {
        fun displayContentLoadingProgress()
        fun dismissContentLoadingProgress()
        fun displayAccountBalance(balance: String?)
        fun displayAccountName(name: String?)
    }

    interface Action : CoreAction {
        var currentLocation: Location?
        fun requestInformationCollection()
    }

}
package org.moklet.wikusama.feature.presenter

import org.moklet.wikusama.controller.ControllerEndpoint
import org.moklet.wikusama.feature.contract.ContractOTP
import org.moklet.wikusama.feature.core.CorePresenter
import org.moklet.wikusama.repository.RepositorySession
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PresenterOTP(private var repositorySession: RepositorySession,
                   private var controllerEndpoint: ControllerEndpoint) : CorePresenter<ContractOTP.View>(), ContractOTP.Action {

    override fun loadDestinationPhoneNumber() {
        viewLayer?.displayPhoneNumber(repositorySession.userEmail)
    }

    override fun requestSendOneTimePass() {
        viewLayer?.displayContentLoadingProgress()
        GlobalScope.launch(job + uiDispatcher) {
//            val response = controllerEndpoint.sendOTPAsync(ModelOTP.RequestSendOTP(repositorySession.userEmail)).await()
//            if (response.isSuccessful) {
                viewLayer?.dismissContentLoadingProgress()
////                repositorySession.appReferenceToken = response.body()?.data?.sessionID
//            }
        }
    }

    override fun requestVerifyOneTimePass(oneTimePass: String?) {
        viewLayer?.displayContentLoadingProgress()
        GlobalScope.launch(job + uiDispatcher) {
//            val response = controllerEndpoint.verifyOPPAsync(ModelOTP.RequestVerifyOTP(repositorySession.appReferenceToken, oneTimePass)).await()
//            if (response.isSuccessful) {
                viewLayer?.dismissContentLoadingProgress()
                checkVerifyOneTimePass("")
//            }
        }
    }

    override fun checkVerifyOneTimePass(session: String?) {
//        checkNotNull(session) {
//            viewLayer?.displayOneTimePassNotVerify()
//            return
//        }
//
//        repositorySession.userInfo = session
        viewLayer?.openDashboard()
    }

    override fun bindingOneTimePass(oneTimePass: String?) {
        viewLayer?.displayOneTimePass(oneTimePass)
    }

    override fun openResendOneTimePass() {
        viewLayer?.displayResendOneTimePass(repositorySession.userEmail)
    }

}
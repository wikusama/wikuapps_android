package org.moklet.wikusama.feature.screen.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.OnClick
import org.moklet.wikusama.R
import org.moklet.wikusama.feature.contract.ContractRegister
import org.moklet.wikusama.feature.core.CoreActivity
import org.moklet.wikusama.feature.presenter.PresenterRegister
import org.moklet.wikusama.utilities.android.UtilConstant
import kotlinx.android.synthetic.main.activity_register.*
import org.koin.android.ext.android.inject

open class ActivityRegister : CoreActivity(), ContractRegister.View {

    companion object {
        const val LAYOUT_RESOURCE = R.layout.activity_register

        fun startRegister(context: Context) {
            val intentRegister = Intent(context, ActivityRegister::class.java)
            intentRegister.action = UtilConstant.WIKU_ACTION_REGISTER
            context.startActivity(intentRegister)
        }
    }

    override fun getLayoutId(): Int = LAYOUT_RESOURCE
    private val presenterRegister: PresenterRegister by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenterRegister.attachView(this)
    }

    @OnClick(R.id.view_register_action_next_button)
    protected fun onOTPValidationSelected() {
        presenterRegister.requestOneTimePassVerification(view_register_msisdn_edit_text?.text.toString())
    }

    override fun openOneTimePassVerification() {
        ActivityOTP.startOneTimePass(this)
        finish()
    }

    override fun displayContentLoadingProgress() {
        showBottomProgress()
    }

    override fun dismissContentLoadingProgress() {
        dismissBottomProgress()
    }

    override fun displayEmailNotValid() {
        view_register_hint_error_textview?.text = getString(R.string.WIKU_HINT_INVALID_MSISDN)
        view_register_hint_error_textview?.visibility = View.VISIBLE
    }

    override fun onDestroy() {
        presenterRegister.detachView()
        super.onDestroy()
    }


}
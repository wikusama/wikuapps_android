package org.moklet.wikusama.feature.presenter

import android.content.Context
import org.moklet.wikusama.feature.contract.ContractSplash
import org.moklet.wikusama.feature.core.CorePresenter
import org.moklet.wikusama.repository.RepositorySession
import org.moklet.wikusama.utilities.android.UtilConstant
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class PresenterSplash(var application: Context,
                      private var repositorySession: RepositorySession)
    : CorePresenter<ContractSplash.View>(), ContractSplash.Action {

    override fun requestCheckSession() {
        when (repositorySession.isLogin) {
            true    -> viewLayer?.openDashboard()
            false   -> viewLayer?.openBoarding()
        }
    }

    override fun requestAnimation() {
        GlobalScope.launch (job + uiDispatcher) {
            delay(UtilConstant.WIKU_DURATION_SPLASH)
            requestCheckSession()
        }
    }

}
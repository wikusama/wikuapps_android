package org.moklet.wikusama.feature.core

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import androidx.fragment.app.Fragment

abstract class CoreFragment : Fragment() {

    protected abstract val getLayoutId : Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(getLayoutId, container, false)
        ButterKnife.bind(this, view)
        return view
    }

}
package org.moklet.wikusama.feature.screen.activity

import android.content.Context
import org.moklet.wikusama.R
import org.moklet.wikusama.feature.core.CoreActivity
import org.moklet.wikusama.utilities.android.UtilConstant
import org.jetbrains.anko.intentFor

open class ActivityBlocking : CoreActivity() {

    companion object {
        const val LAYOUT_RESOURCE = R.layout.activity_splash

        fun newInstanceBlockingError(context: Context, blockingCode: Long) {
            context.startActivity(context.intentFor<ActivityBlocking>(
                    UtilConstant.WIKU_ARGUMENT_ACTION to UtilConstant.WIKU_ACTION_BLOCKING_ERROR,
                    UtilConstant.WIKU_ARGUMENT_BLOCKING_CODE to blockingCode))
        }

    }

    override fun getLayoutId(): Int = LAYOUT_RESOURCE

}
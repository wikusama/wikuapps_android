package org.moklet.wikusama.feature.screen.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.view.GravityCompat
import butterknife.OnClick
import org.moklet.wikusama.R
import org.moklet.wikusama.feature.contract.ContractDashboard
import org.moklet.wikusama.feature.core.CoreActivity
import org.moklet.wikusama.feature.presenter.PresenterDashboard
import org.moklet.wikusama.utilities.android.UtilConstant
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.view_dashboard_home.*
import kotlinx.android.synthetic.main.view_dashboard_toolbox.*
import org.koin.android.ext.android.inject

open class ActivityDashboard : CoreActivity(), ContractDashboard.View {


    companion object {
        const val LAYOUT_RESOURCE = R.layout.activity_dashboard
        const val CLASS_TAG = "Activity Dashboard"

        fun startDashboard(context: Context) {
            val intentNotification = Intent(context, ActivityDashboard::class.java)
            intentNotification.action = UtilConstant.WIKU_ACTION_DASHBOARD
            context.startActivity(intentNotification)
        }
    }

    override fun getLayoutId(): Int = LAYOUT_RESOURCE
    private val presenterDashboard: PresenterDashboard by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializePresenter()
    }

    private fun initializePresenter() {
        presenterDashboard.attachView(this)
        presenterDashboard.requestInformationCollection()
    }

    @OnClick(R.id.view_dashboard_action_toggle_menu_fab)
    protected fun onToggleMenuSelected() {
        if (drawer_layout?.isDrawerOpen(GravityCompat.START) == true) drawer_layout?.closeDrawer(GravityCompat.START)
        else drawer_layout?.openDrawer(GravityCompat.START)
    }

    @OnClick(R.id.view_dashboard_feature_car_image_button)
    protected fun onStartRideMotorcycleServiceSelected() {
        showErrorMessage(getString(R.string.WIKU_HINT_IS_UNDER_DEVELOPMENT))
    }

    @OnClick(R.id.view_dashboard_feature_motorcycle_image_button)
    protected fun onStartRideCarServiceSelected() {
        showErrorMessage(getString(R.string.WIKU_HINT_IS_UNDER_DEVELOPMENT))
    }

    @OnClick(R.id.view_dashboard_feature_food_image_button)
    protected fun onStartFoodServiceSelected() {
        showErrorMessage(getString(R.string.WIKU_HINT_IS_UNDER_DEVELOPMENT))
    }

    @OnClick(R.id.view_dashboard_feature_travel_image_button)
    protected fun onStartTravelServiceSelected() {
        showErrorMessage(getString(R.string.WIKU_HINT_IS_UNDER_DEVELOPMENT))
    }

    @OnClick(R.id.view_dashboard_feature_market_image_button)
    protected fun onStartMarketServiceSelected() {
        showErrorMessage(getString(R.string.WIKU_HINT_IS_UNDER_DEVELOPMENT))
    }

    override fun displayContentLoadingProgress() {
        showBottomProgress()
    }

    override fun dismissContentLoadingProgress() {
        dismissBottomProgress()
    }

    override fun displayAccountName(name: String?) {
        view_dashboard_home_greeting_textview?.text = getString(R.string.WIKU_TITLE_GREETING, name)
    }

    override fun displayAccountBalance(balance: String?) {
        view_dashboard_home_account_balance_textview?.text = balance
    }


    override fun onDestroy() {
        presenterDashboard.detachView()
        super.onDestroy()
    }

}
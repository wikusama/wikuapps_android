package org.moklet.wikusama.feature.screen.dialog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import butterknife.OnClick
import org.moklet.wikusama.R
import org.moklet.wikusama.feature.core.CoreBottomSheet
import org.moklet.wikusama.utilities.android.UtilConstant
import org.moklet.wikusama.utilities.kotlin.argument
import org.moklet.wikusama.utilities.kotlin.withArguments
import kotlinx.android.synthetic.main.dialog_resend_otp.*

open class DialogResendOTP : CoreBottomSheet() {

    companion object {
        const val LAYOUT_RESOURCE   = R.layout.dialog_resend_otp
        const val CLASS_TAG         = "Dialog Resend OTP"

        @JvmStatic
        fun newInstance(targetFragment: Fragment?, origin: String, action: String, msisdn: String?): DialogResendOTP {
            val dialogResendOTP = DialogResendOTP()
            dialogResendOTP.isExpandableDialog = false
            dialogResendOTP.setTargetFragment(targetFragment, 0)
            dialogResendOTP.withArguments(
                    UtilConstant.WIKU_ARGUMENT_ORIGIN to origin,
                    UtilConstant.WIKU_ARGUMENT_ACTION to action,
                    UtilConstant.WIKU_ARGUMENT_MSISDN to msisdn)
            return dialogResendOTP
        }
    }

    interface DialogResendOTPCallback {
        fun onResendByPhoneSelected()
    }

    override val getLayoutId: Int = LAYOUT_RESOURCE
    private val msisdn by argument<String>(UtilConstant.WIKU_ARGUMENT_MSISDN)
    private var dialogResendOTPCallback: DialogResendOTPCallback? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialogResendOTPCallback =  (targetFragment ?: activity) as? DialogResendOTPCallback
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initializeDestination()
    }

    private fun initializeDestination() {
        view_resend_otp_destination_textview?.text = getString(R.string.WIKU_FORM_BUILDER_OTP, msisdn)
    }

    @OnClick(R.id.view_resend_otp_action_by_phone_textview)
    protected fun onActionResendOTPByPhone() {
        dialogResendOTPCallback?.onResendByPhoneSelected()
    }

    override fun onDetach() {
        dialogResendOTPCallback = null
        super.onDetach()
    }



}
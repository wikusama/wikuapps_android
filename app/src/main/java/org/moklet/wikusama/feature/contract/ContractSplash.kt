package org.moklet.wikusama.feature.contract

import org.moklet.wikusama.feature.core.CoreView
import org.moklet.wikusama.feature.core.CoreAction

object ContractSplash {

    interface View : CoreView {
        fun displayAnimationSplash()
        fun displayAppVersion(appVersion: String?)
        fun openDashboard()
        fun openBoarding()
    }

    interface Action : CoreAction {
        fun requestAnimation()
        fun requestCheckSession()
    }

}
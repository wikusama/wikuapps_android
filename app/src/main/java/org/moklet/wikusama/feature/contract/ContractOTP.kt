package org.moklet.wikusama.feature.contract

import org.moklet.wikusama.feature.core.CoreView
import org.moklet.wikusama.feature.core.CoreAction
import org.moklet.wikusama.model.ModelUser

object ContractOTP {

    interface View : CoreView {
        fun displayPhoneNumber(phoneNumber: String?)
        fun displayOneTimePass(oneTimePass: String?)
        fun displayContentLoadingProgress()
        fun dismissContentLoadingProgress()
        fun displayResendOneTimePass(phoneNumber: String?)
        fun displayOneTimePassNotVerify()
        fun openDashboard()
    }

    interface Action : CoreAction {
        fun requestSendOneTimePass()
        fun openResendOneTimePass()
        fun requestVerifyOneTimePass(oneTimePass: String?)
        fun bindingOneTimePass(oneTimePass: String?)
        fun checkVerifyOneTimePass(session: String?)
        fun loadDestinationPhoneNumber()
    }

}
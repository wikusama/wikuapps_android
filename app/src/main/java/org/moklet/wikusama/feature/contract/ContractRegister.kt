package org.moklet.wikusama.feature.contract

import org.moklet.wikusama.feature.core.CoreView
import org.moklet.wikusama.feature.core.CoreAction

object ContractRegister {

    interface View : CoreView {
        fun displayContentLoadingProgress()
        fun dismissContentLoadingProgress()
        fun displayEmailNotValid()
        fun openOneTimePassVerification()
    }

    interface Action : CoreAction {
        fun requestOneTimePassVerification(phoneNumber: String?)
    }

}
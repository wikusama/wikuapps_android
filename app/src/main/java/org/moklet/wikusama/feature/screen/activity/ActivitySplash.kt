package org.moklet.wikusama.feature.screen.activity

import android.os.Bundle
import org.moklet.wikusama.R
import org.moklet.wikusama.feature.contract.ContractSplash
import org.moklet.wikusama.feature.core.CoreActivity
import org.moklet.wikusama.feature.presenter.PresenterSplash
import org.koin.android.ext.android.inject

open class ActivitySplash : CoreActivity(), ContractSplash.View {

    companion object {
        const val LAYOUT_RESOURCE = R.layout.activity_splash
        const val CLASS_TAG = "Activity Splash"
    }

    val presenterSplash: PresenterSplash by inject()

    override fun getLayoutId(): Int = LAYOUT_RESOURCE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializePresenter()
    }

    private fun initializePresenter() {
        presenterSplash.attachView(this)
        presenterSplash.requestAnimation()
    }

    override fun displayAnimationSplash() {

    }

    override fun openDashboard() {
        ActivityDashboard.startDashboard(this)
        finish()
    }

    override fun openBoarding() {
        ActivityRegister.startRegister(this)
        finish()
    }

    override fun displayAppVersion(appVersion: String?) {

    }

    override fun onDestroy() {
        presenterSplash.detachView()
        super.onDestroy()
    }

}
package org.moklet.wikusama.feature.core

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import butterknife.ButterKnife
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.moklet.wikusama.utilities.android.UtilConstant

abstract class CoreBottomSheet : BottomSheetDialogFragment() {

    protected abstract val getLayoutId : Int
    var isExpandableFully : Boolean = false
    var isExpandableDialog : Boolean = false
    var collapseHeight : Float = 0.5F
    var behavior: BottomSheetBehavior<FrameLayout>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(getLayoutId, container, false)
        setContentHeight(view)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog = super.onCreateDialog(savedInstanceState) as com.google.android.material.bottomsheet.BottomSheetDialog
        bottomSheetDialog.setOnShowListener {
            bottomSheetDialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)?.run {
                behavior = BottomSheetBehavior.from(bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet))
                val displayMetrics = activity?.resources?.displayMetrics
                if(isExpandableDialog){
                    behavior?.state = com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_COLLAPSED
                    behavior?.peekHeight = (displayMetrics?.heightPixels?.times(collapseHeight))?.toInt() ?: UtilConstant.WIKU_INITIAL_POSITION
                } else{
                    behavior?.state = com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
                    if(isExpandableFully){
                        behavior?.peekHeight = 0
                    }
                }
            }
        }
        return bottomSheetDialog
    }

    private fun setContentHeight(view: View?) {
        if((isExpandableDialog or isExpandableFully) && collapseHeight == 0.5F){
            val displayMetrics = activity?.resources?.displayMetrics
            view?.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, displayMetrics?.heightPixels ?: 0)
        }
    }

}
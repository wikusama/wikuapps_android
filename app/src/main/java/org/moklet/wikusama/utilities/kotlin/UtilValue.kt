package org.moklet.wikusama.utilities.kotlin

fun String?.ifNotNullOrEmpty(func: (String?.() -> Unit)? = null) {
    if (!this.isNullOrEmpty()) {
        func?.invoke(this)
    }
}

fun String?.ifNullOrEmpty(defaultValue: String? = this, func: (String?.() -> Unit)? = null): String {
    return if (this == null || this.isEmpty()) {
        func?.invoke(this)
        defaultValue.toString()
    }
    else {
        this
    }
}

fun <T> List<T>?.ifNotNullOrEmpty(func: List<T>.() -> Unit) {
    if (this != null && !this.isEmpty()) {
        func()
    }
}

fun List<Any>?.ifNullOrEmpty(func: () -> Unit) {
    if (this == null || this.isEmpty()) {
        func()
    }
}
package org.moklet.wikusama.utilities.kotlin

import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

fun Number.formatNumber(): String {
    val formatter = DecimalFormat("#,###,###").apply { roundingMode = RoundingMode.HALF_UP }
    val yourFormattedString = formatter.format(this)
    return yourFormattedString.replace(",".toRegex(), ".")
}

fun String.formatNumber(): String {
    try {
        val symbol = DecimalFormatSymbols()
        symbol.decimalSeparator = ','
        val decimalFormat = DecimalFormat("###,###,###,###", symbol)
        return decimalFormat.format(Integer.parseInt(this)).replace(",".toRegex(), ".")
    } catch (exception: Exception) { }
    return this
}

fun String.withCurrency(currency: String?): SpannableString {
    val content = SpannableString("$currency $this")
    content.setSpan(RelativeSizeSpan(0.6f), 0, 3, 0)
    return content
}
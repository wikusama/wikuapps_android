package org.moklet.wikusama.utilities.android

import org.moklet.wikusama.BuildConfig

object UtilConstant {

    const val WIKU_INITIAL_POSITION = 0
    const val WIKU_INITIAL_EMPTY = ""
    const val WIKU_DURATION_SPLASH = 1500L
    const val WIKU_INITIAL_MAX_MSISDN = 15
    const val WIKU_INITIAL_MIN_MSISDN = 8
    const val WIKU_INITIAL_PATH = "wikusama.org"

    const val WIKU_ARGUMENT_ACTION = "argument_action"
    const val WIKU_ARGUMENT_BLOCKING_CODE = "argument_blocking_code"
    const val WIKU_ARGUMENT_ORIGIN = "argument_origin"
    const val WIKU_ARGUMENT_TYPE = "argument_type"
    const val WIKU_ARGUMENT_MSISDN = "argument_msisdn"

    const val WIKU_TAG_ACCEPT_CONTENT = "Accept"
    const val WIKU_TAG_AUTHORIZATION = "Authorization"
    const val WIKU_TAG_OS_VERSION = "Os-Version"
    const val WIKU_TAG_OS_TYPE = "Os-Type"
    const val WIKU_TAG_APP_VERSION = "App-Version"
    const val WIKU_TAG_DEVICE_NAME = "Device-Name"
    const val WIKU_TAG_SESSION_TYPE = "uTipe"
    const val WIKU_TAG_DEVICE_SERIAL = "Device-Serial"

    const val WIKU_HEADER_APP_VERSION = BuildConfig.VERSION_CODE.toString()
    const val WIKU_HEADER_CONTENT_TYPE = "application/json"
    const val WIKU_HEADER_USER_TYPE = "1"

    const val WIKU_ACTION_BLOCKING_ERROR = "action_blocking_error"
    const val WIKU_ACTION_DASHBOARD = "action_dashboard"
    const val WIKU_ACTION_ONE_TIME_PASS = "action_one_time_pass"
    const val WIKU_ACTION_REGISTER = "action_register"
    const val WIKU_ACTION_NOTIFICATION = "action_notification"
    const val WIKU_ACTION_RESEND_OTP = "action_resend_otp"

}
package org.moklet.wikusama.utilities.android

import android.text.TextUtils
import org.moklet.wikusama.utilities.android.UtilConstant


object UtilFormat {

    fun isValidMSISDN(phoneNumber: String?) : Boolean {
        check(phoneNumber?.isNotBlank() == true) {
            return false
        }
        check (phoneNumber?.length ?: UtilConstant.WIKU_INITIAL_POSITION <= UtilConstant.WIKU_INITIAL_MAX_MSISDN) {
            return false
        }
        check (phoneNumber?.length ?: UtilConstant.WIKU_INITIAL_POSITION > UtilConstant.WIKU_INITIAL_MIN_MSISDN) {
            return false
        }
        return true
    }

    fun isValidEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

}
package org.moklet.wikusama.utilities.android

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import org.moklet.wikusama.utilities.android.UtilConstant

object UtilStorage {

    @SuppressLint("Recycle")
    fun getPath(context: Context, uri: Uri): String {
        try {
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor.moveToFirst()
            val columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA)
            return cursor.getString(columnIndex)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return UtilConstant.WIKU_INITIAL_EMPTY
    }

}

package org.moklet.wikusama.utilities.android

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings

object UtilSystem {

    fun getAndroidVersionName(): String {
        return android.os.Build.VERSION.RELEASE
    }

    @SuppressLint("HardwareIds")
    fun getDeviceUniqueID(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

}
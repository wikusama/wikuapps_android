package org.moklet.wikusama.utilities.kotlin

import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

fun compressMultiFiles(args: ArrayList<String>, filename: String) {
    ZipOutputStream(BufferedOutputStream(FileOutputStream(filename))).use { out ->
        for (file in args) {
            FileInputStream(file).use { fi ->
                BufferedInputStream(fi).use { origin ->
                    val entry = ZipEntry(file.substring(file.lastIndexOf("/")))
                    out.putNextEntry(entry)
                    origin.copyTo(out, 1024)
                }
            }
        }
    }
}
package org.moklet.wikusama.utilities.android

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.util.Base64
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatDrawableManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import org.moklet.wikusama.R
import org.moklet.wikusama.controller.GlideApp
import java.io.ByteArrayOutputStream
import android.graphics.drawable.VectorDrawable
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import android.graphics.drawable.BitmapDrawable
import androidx.core.content.ContextCompat
import android.os.Build
import android.annotation.TargetApi
import android.graphics.Canvas
import androidx.annotation.RequiresApi

object UtilImage {

    fun getNotificationIcon(): Int {
        return R.drawable.ic_logo_launcher_24dp
    }

    fun loadImageWithoutPlaceholder(view: ImageView?, imagePath: String?, context: Context) {
        val safeImagePath = if (!imagePath.isNullOrBlank()) imagePath else UtilConstant.WIKU_INITIAL_PATH
        view?.let { GlideApp.with(context).load(safeImagePath).placeholder(R.color.WIKU_COLOR_BACKGROUND_PLACEHOLDER).into(view) }
    }

    @SuppressLint("RestrictedApi")
    fun loadImageWithCirclePlaceholder(view: ImageView?, imagePath: String?, context: Context, @DrawableRes placeholder: Int) {
        val safeImagePath = if (!imagePath.isNullOrBlank()) imagePath else UtilConstant.WIKU_INITIAL_PATH
        val drawable = context.let { AppCompatDrawableManager.get().getDrawable(it, placeholder) }
        view?.let { GlideApp.with(context).load(safeImagePath).transforms(CenterCrop(), CircleCrop()).placeholder(drawable).into(it) }
    }

    @SuppressLint("RestrictedApi")
    fun loadImageWithCirclePlaceholderNoCache(view: ImageView?, imagePath: String?, context: Context, @DrawableRes placeholder: Int) {
        val safeImagePath = if (!imagePath.isNullOrBlank()) imagePath else UtilConstant.WIKU_INITIAL_PATH
        val drawable = context.let { AppCompatDrawableManager.get().getDrawable(it, placeholder) }
        view?.let { GlideApp.with(context).load(safeImagePath).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).transforms(CenterCrop(), CircleCrop()).placeholder(drawable).into(it) }
    }

    @SuppressLint("RestrictedApi")
    fun loadImageWithCirclePlaceholder(view: ImageView?, imagePath: Uri?, context: Context, @DrawableRes placeholder: Int) {
        val drawable = context.let { AppCompatDrawableManager.get().getDrawable(it, placeholder) }
        view?.let { GlideApp.with(context).load(imagePath).transforms(CenterCrop(), CircleCrop()).placeholder(drawable).into(it) }
    }

    fun loadImageCenterWithoutPlaceholder(view: ImageView?, imagePath: String?, context: Context) {
        val safeImagePath = if (!imagePath.isNullOrBlank()) imagePath else UtilConstant.WIKU_INITIAL_PATH
        view?.let { GlideApp.with(context).load(safeImagePath).transform(CenterCrop()).placeholder(R.color.WIKU_COLOR_BACKGROUND_PLACEHOLDER).into(view) }
    }

    fun loadImageCenterWithoutCache(view: ImageView?, imagePath: String?, context: Context) {
        val safeImagePath = if (!imagePath.isNullOrBlank()) imagePath else UtilConstant.WIKU_INITIAL_PATH
        view?.let { GlideApp.with(context).load(safeImagePath).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).transform(CenterCrop()).placeholder(R.color.WIKU_COLOR_BACKGROUND_PLACEHOLDER).into(view) }
    }

    fun getBase64FromBitmap(bitmap: Bitmap?): String? {
        val bitOutputStream = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.JPEG, 60, bitOutputStream)
        return Base64.encodeToString(bitOutputStream.toByteArray(), Base64.DEFAULT)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun getBitmap(vectorDrawable: VectorDrawable?): Bitmap {
        val bitmap = Bitmap.createBitmap(vectorDrawable?.intrinsicWidth ?: 0, vectorDrawable?.intrinsicHeight ?: 0, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable?.setBounds(0, 0, canvas.width, canvas.height)
        vectorDrawable?.draw(canvas)
        return bitmap
    }

    private fun getBitmap(vectorDrawable: VectorDrawableCompat?): Bitmap {
        val bitmap = Bitmap.createBitmap(vectorDrawable?.intrinsicWidth ?: 0, vectorDrawable?.intrinsicHeight ?: 0, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable?.setBounds(0, 0, canvas.width, canvas.height)
        vectorDrawable?.draw(canvas)
        return bitmap
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun getBitmapFromVector(context: Context, @DrawableRes drawableResId: Int): Bitmap {
        val drawable = ContextCompat.getDrawable(context, drawableResId)
        return when (drawable) {
            is BitmapDrawable -> drawable.bitmap
            is VectorDrawableCompat -> getBitmap((drawable as VectorDrawableCompat?))
            is VectorDrawable -> getBitmap((drawable as? VectorDrawable?))
            else -> throw IllegalArgumentException("Unsupported drawable type")
        }
    }

}

fun Bitmap.getResizedBitmap( maxSize:Int) : Bitmap {
    var width = this.width
    var height = this.height
    val  bitmapRatio : Float =  width.toFloat()/  height.toFloat()
    if (bitmapRatio > 1) {
        width = maxSize
        height =  (width / bitmapRatio).toInt()
    } else {
        height = maxSize
        width = (height * bitmapRatio).toInt()
    }
    return Bitmap.createScaledBitmap(this, width, height, true)
}
@file:Suppress("UNCHECKED_CAST")

package org.moklet.wikusama.utilities.kotlin

import android.os.Bundle
import java.io.Serializable

fun androidx.fragment.app.Fragment.withArguments(vararg arguments: Pair<String, Serializable?>): androidx.fragment.app.Fragment {
    val bundle = Bundle()
    arguments.forEach { bundle.putSerializable(it.first, it.second) }
    this.arguments = bundle
    return this
}

fun <T : Any?> androidx.fragment.app.FragmentActivity.argument(key: String, defaultValue: T? = null) = lazy { intent.extras[key] as? T ?: defaultValue }

fun <T : Any?> androidx.fragment.app.Fragment.argument(key: String, defaultValue: T? = null) = lazy { arguments?.get(key)  as? T ?: defaultValue }

fun <T : Any?> androidx.fragment.app.FragmentActivity.argument(key: String) = lazy { intent.extras[key] as? T }

fun <T : Any?> androidx.fragment.app.Fragment.argument(key: String) = lazy { arguments?.get(key) as? T }



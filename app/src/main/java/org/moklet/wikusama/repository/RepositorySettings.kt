package org.moklet.wikusama.repository

import org.moklet.wikusama.controller.ControllerPreference

class RepositorySettings(controllerPreferences: ControllerPreference?) {

    companion object {
        var controllerPreference: ControllerPreference? = null
        const val SETTINGS_APP_VERSION_CODE = "app_version_code"
        const val SETTINGS_APP_VERSION_NAME = "app_version_name"
        const val SETTINGS_APP_TOKEN = "app_token"
    }

    init {
        controllerPreference = controllerPreferences
    }

    var appVersionCode: Int?
        get() {
            return controllerPreference?.getInt(SETTINGS_APP_VERSION_CODE, 0)
        }
        set(value) {
            value?.let { controllerPreference?.setPref(SETTINGS_APP_VERSION_CODE, it) }
        }

    var appVersionName: String?
        get() {
            return controllerPreference?.getString(SETTINGS_APP_VERSION_NAME, "")
        }
        set(value) {
            value?.let { controllerPreference?.setPref(SETTINGS_APP_VERSION_NAME, it) }
        }


}
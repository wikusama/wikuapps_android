package org.moklet.wikusama.repository

import com.google.gson.Gson
import org.moklet.wikusama.controller.ControllerPreference
import org.moklet.wikusama.model.ModelUser
import org.moklet.wikusama.utilities.android.UtilConstant

class RepositorySession(controllerPreferences: ControllerPreference?) {

    companion object {
        var controllerPreference: ControllerPreference? = null
        const val SESSION_USER_EMAIL = "user_email"
        const val SESSION_USER_INFO = "user_info"
    }

    init {
        controllerPreference = controllerPreferences
    }

    var isLogin: Boolean = false
        get() {
            return false
        }

    var userEmail: String?
        get() {
            return controllerPreference?.getString(SESSION_USER_EMAIL, UtilConstant.WIKU_INITIAL_EMPTY)
        }
        set(value) {
            value?.let { controllerPreference?.setPref(SESSION_USER_EMAIL, it) }
        }


    fun clearUserSession() {
        controllerPreference?.clearPref(SESSION_USER_INFO)
        controllerPreference?.clearPref(SESSION_USER_EMAIL)
    }


}
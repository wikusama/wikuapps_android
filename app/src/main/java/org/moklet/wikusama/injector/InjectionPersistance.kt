package org.moklet.wikusama.injector

import org.moklet.wikusama.controller.ControllerPreference
import org.moklet.wikusama.repository.RepositorySession
import org.moklet.wikusama.repository.RepositorySettings
import org.koin.dsl.module.applicationContext

val modulePersistence = applicationContext {
    bean { ControllerPreference(get()) }
    bean { RepositorySettings(get()) }
    bean { RepositorySession(get()) }
}

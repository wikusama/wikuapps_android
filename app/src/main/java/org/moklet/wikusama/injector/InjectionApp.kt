package org.moklet.wikusama.injector

import org.moklet.wikusama.feature.presenter.*
import org.koin.dsl.module.applicationContext

val moduleApp = applicationContext {

    factory { PresenterSplash                   (get(), get()) }
    factory { PresenterRegister                 (get()) }
    factory { PresenterOTP                      (get(), get()) }
    factory { PresenterDashboard                (get(), get()) }

}
package org.moklet.wikusama.injector

import org.moklet.wikusama.controller.ControllerRest
import org.moklet.wikusama.controller.ControllerSSL
import org.koin.dsl.module.applicationContext

val moduleRemote = applicationContext {
    bean { ControllerSSL.newInstance(get()) }
    bean { ControllerRest().newInstance(get(), get(), get(), get()) }
}

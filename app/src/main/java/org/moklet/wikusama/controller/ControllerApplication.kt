package org.moklet.wikusama.controller

import android.app.Application
import org.moklet.wikusama.injector.moduleApp
import org.moklet.wikusama.injector.modulePersistence
import org.moklet.wikusama.injector.moduleRemote
import org.koin.android.ext.android.startKoin

class ControllerApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(moduleRemote, moduleApp, modulePersistence))
    }

}

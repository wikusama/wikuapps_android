package org.moklet.wikusama.controller

import org.moklet.wikusama.model.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface ControllerEndpoint {

    @POST("user/sign/in/")
    fun sendOTPAsync(@Body payload: ModelOTP.RequestSendOTP): Deferred<Response<ModelOTP.ResponseSendOTP>>

    @POST("user/sign/in/verify")
    fun verifyOPPAsync(@Body payload: ModelOTP.RequestVerifyOTP): Deferred<Response<ModelOTP.ResponseVerifyOTP>>


}
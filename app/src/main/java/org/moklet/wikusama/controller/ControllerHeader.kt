package org.moklet.wikusama.controller

import android.app.Application
import android.util.Base64
import org.moklet.wikusama.repository.RepositorySession
import org.moklet.wikusama.repository.RepositorySettings
import org.moklet.wikusama.utilities.android.UtilConstant
import org.moklet.wikusama.utilities.android.UtilSystem
import okhttp3.Interceptor
import okhttp3.Response

class ControllerHeader() : Interceptor {

    private lateinit var repositorySession: RepositorySession
    private lateinit var repositorySettings: RepositorySettings
    private lateinit var application: Application

    constructor(repositorySession: RepositorySession, repositorySettings: RepositorySettings, application: Application) : this() {
        this.repositorySession = repositorySession
        this.repositorySettings = repositorySettings
        this.application = application
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val oriReq = chain.request()
        val newReq = oriReq.newBuilder()
                .addHeader(UtilConstant.WIKU_TAG_AUTHORIZATION, basicAuthentication("wikuapps", "ETQxzkykwCtd7yhF"))
                .addHeader(UtilConstant.WIKU_TAG_ACCEPT_CONTENT, UtilConstant.WIKU_HEADER_CONTENT_TYPE)
                .addHeader(UtilConstant.WIKU_TAG_DEVICE_NAME, android.os.Build.MODEL)
                .addHeader(UtilConstant.WIKU_TAG_OS_VERSION, UtilSystem.getAndroidVersionName())
                .addHeader(UtilConstant.WIKU_TAG_OS_TYPE, android.os.Build.VERSION.SDK_INT.toString())
                .addHeader(UtilConstant.WIKU_TAG_SESSION_TYPE, UtilConstant.WIKU_HEADER_USER_TYPE)
                .addHeader(UtilConstant.WIKU_TAG_APP_VERSION, UtilConstant.WIKU_HEADER_APP_VERSION)
                .addHeader(UtilConstant.WIKU_TAG_DEVICE_SERIAL, UtilSystem.getDeviceUniqueID(application))
                .build()
        return chain.proceed(newReq)
    }

    private fun basicAuthentication(email: String?, password: String?): String {
        return "Basic " + Base64.encodeToString("$email:$password".toByteArray(charset("UTF-8")), Base64.NO_WRAP)
    }

}
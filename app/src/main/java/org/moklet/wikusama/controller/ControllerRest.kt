package org.moklet.wikusama.controller

import android.app.Application
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import org.moklet.wikusama.repository.RepositorySession
import org.moklet.wikusama.repository.RepositorySettings
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.jvm.java

class ControllerRest {

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    fun newInstance(controllerSSL: ControllerSSL, repositorySession: RepositorySession,
                    repositorySettings: RepositorySettings, application: Application): ControllerEndpoint {
        val converterBuilder = GsonBuilder().apply { serializeNulls() }
        val client = OkHttpClient.Builder()

        //client.certificatePinner(certificatePinier)
        client.addInterceptor(ControllerHeader(repositorySession, repositorySettings, application))
        client.addInterceptor(logInterceptor)

        val retrofit = Retrofit.Builder()
                .baseUrl("https://wikuapps-api.wikufest.org/")
                .addConverterFactory(GsonConverterFactory.create(converterBuilder.create()))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(client.build())
                .build()

        return retrofit.create(ControllerEndpoint::class.java)
    }

    private val logInterceptor: HttpLoggingInterceptor
        get() {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            return httpLoggingInterceptor
        }

}
